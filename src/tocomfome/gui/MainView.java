/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocomfome.gui;

import static java.lang.Double.parseDouble;
import tocomfome.controller.ControlClient;
import tocomfome.controller.ControlDessert;
import tocomfome.controller.ControlDish;
import tocomfome.controller.ControlDrink;
import tocomfome.controller.ControlOrder;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import tocomfome.models.Client;
import tocomfome.models.Dessert;
import tocomfome.models.Dish;
import tocomfome.models.Drink;
import tocomfome.models.Order;
import tocomfome.models.Stock;



public class MainView extends javax.swing.JFrame {
    private int estadoAlteracao=1;
    private JFrame callerJFrame;
    private boolean modoAlteracao=false;
    private Client client;
    private ControlClient controlClient;
    private Client clientePedido;        
    private boolean novoRegistro;
    private Order order;
    private ControlOrder controlOrder;
    
    private Dish dish;
    private ControlDish controlDish;
    private Drink drink;
    private ControlDrink controlDrink;
    private Dessert dessert;
    private ControlDessert controlDessert;
    private boolean novoPedido;
    private boolean modoAlteracaoPedido;
    private int estadoAlteracaoPedido=1;
    private Dish dishSelect;
    private Drink drinkSelect;
    private Dessert dessertSelect;
    private boolean clicouPrato=false;
    private boolean clicouBebida=false;
    private boolean clicouSobremesa=false;
    
    public MainView(JFrame callerJFrame) {
        initComponents();
        HabilitarDesabilitarCampos();
        controlClient = new ControlClient();
        controlDish = new ControlDish();
        controlDrink = new ControlDrink();
        controlDessert = new ControlDessert();
        controlOrder = new ControlOrder();
        this.callerJFrame = callerJFrame;
        carregarProductList();
        carregarClientList();
        EstadoAlteracao();
        EstadoAlteracaoPedido();
        
    }

    public void HabilitarDesabilitarCampos(){
        
            jTextFieldNomeCliente.setEnabled(modoAlteracao);
            jTextFieldCpf.setEnabled(modoAlteracao);
            
            modoAlteracao=true;
    }
    public void limparCampos(){
        jTextFieldNomeCliente.setText("");
        jTextFieldCpf.setText("");
    }
    public void salvarRegistro(){
        
        if(jTextFieldNomeCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "É necessário preencher o nome do cliente");
        }
        else if(jTextFieldCpf.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "É necessário preencher o CPF");
        }
        else{
            
            if(novoRegistro==true){
                client = new Client(jTextFieldNomeCliente.getText(), jTextFieldCpf.getText());
            }
            else{
                client.setName(jTextFieldNomeCliente.getText());
                client.setCpf(jTextFieldCpf.getText());
            }
            if(novoRegistro==true){
                controlClient.add(client);
            }

            modoAlteracao = false;
            novoRegistro = false;
            estadoAlteracao=1;
            EstadoAlteracao();
            controlClient.escreverArquivoCliente();
            carregarClientList();
            limparCampos();
            HabilitarDesabilitarCampos();
        }
        
    }
    public void preencherCampos(){
        jTextFieldNomeCliente.setText(client.getName());
        jTextFieldCpf.setText(client.getCpf());
    }
        public void pesquisarCliente(String nome) {
        Client clientePesquisado = controlClient.Search(nome);

        if (clientePesquisado == null) {
            exibirInformacao("cliente não encontrado.");
        } else {
            this.client = clientePesquisado;
            this.preencherCampos();
            //this.HabilitarDesabilitarCampos();
        }
    }
        public void exibirInformacao(String info) {
            JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
        }
        public void carregarClientList() {
            controlClient.lerArquivoCliente();
            ArrayList<Client> clientList = controlClient.getClientList();
            DefaultTableModel model = (DefaultTableModel) jTableClientesCadastrados.getModel();
            model.setRowCount(0);
            for (Client b : clientList) {
                model.addRow(new String[]{b.getName(), b.getCpf()});
            }
            jTableClientesCadastrados.setModel(model);
            jTableClientePedido.setModel(model);
        }
        public void CarregarListaPedido(){
            ArrayList<Dish> orderDish = order.getDish();
            ArrayList<Drink> orderDrink = order.getDrink();
            ArrayList<Dessert>orderDessert = order.getDessert();
            DefaultTableModel model = (DefaultTableModel) jTablePedido.getModel();
            model.setRowCount(0);
            for(Dish a : orderDish ){
                model.addRow(new String[]{a.getName(), a.getPrice().toString()});
            }
            for(Drink a : orderDrink){
                model.addRow(new String[]{a.getName(), a.getPrice().toString()});
            }
            for(Dessert a : orderDessert){
                model.addRow(new String[]{a.getName(), a.getPrice().toString()});
            }
            Double preco=0.0;
            Double precoTotal=0.0;
            for(int i=0; i<model.getRowCount();i++){
                preco=parseDouble((String) model.getValueAt(i, 1));
                precoTotal=precoTotal+preco;
            }
            jLabelPrecoTotal.setText(precoTotal.toString());
            order.setTotalValue(precoTotal);
        }
        public void carregarProductList() {
            controlDish.lerArquivoPratos();
            controlDrink.lerArquivoBebidas();
            controlDessert.lerArquivoSobremesas();
            Stock stockDish;
            Stock stockDrink;
            Stock stockDessert;
            ArrayList<Dish> dishList = controlDish.getDishList();
            ArrayList<Drink> drinkList = controlDrink.getDrinkList();
            ArrayList<Dessert> dessertList = controlDessert.getDessertList();
            DefaultTableModel modelDish = (DefaultTableModel) jTablePrato.getModel();
            DefaultTableModel modelDishLista = (DefaultTableModel) jTableListaPratos.getModel();
            DefaultTableModel modelDrink = (DefaultTableModel) jTableBebida.getModel();
            DefaultTableModel modelDrinkLista = (DefaultTableModel) jTableListaBebidas.getModel();
            DefaultTableModel modelDessert = (DefaultTableModel) jTableSobremesa.getModel();
            DefaultTableModel modelDessertLista = (DefaultTableModel) jTableListaSobremesas.getModel();
            modelDish.setRowCount(0);
            modelDishLista.setRowCount(0);
            modelDrink.setRowCount(0);
            modelDrinkLista.setRowCount(0);
            modelDessert.setRowCount(0);
            modelDessertLista.setRowCount(0);
            for (Dish b : dishList) {
                stockDish=b.getStock();
                modelDish.addRow(new String[]{b.getName(), b.getPrice().toString()});
                modelDishLista.addRow(new String[]{b.getName(), b.getPrice().toString(), stockDish.getAmount().toString(), stockDish.getMinimumAmount().toString() });
            }
            for(Drink a : drinkList){
                stockDrink=a.getStock();
                modelDrink.addRow(new String[]{a.getName(), a.getPrice().toString()});
                modelDrinkLista.addRow(new String[]{a.getName(), a.getPrice().toString(), String.valueOf(stockDrink.getAmount()), String.valueOf(stockDrink.getMinimumAmount()) });
            }
            for(Dessert c : dessertList){
                stockDessert=c.getStock();
                modelDessert.addRow(new String[]{c.getName(), c.getPrice().toString()});
                modelDessertLista.addRow(new String[]{c.getName(), c.getPrice().toString(), stockDessert.getAmount().toString(), stockDessert.getMinimumAmount().toString() });
            }
            jTablePrato.setModel(modelDish);
            jTableListaPratos.setModel(modelDishLista);
            jTableBebida.setModel(modelDrink);
            jTableListaBebidas.setModel(modelDrinkLista);
            jTableSobremesa.setModel(modelDessert);
            jTableListaSobremesas.setModel(modelDessertLista);
            
            //jTableClientePedido.setModel(model);
        }
        public void EstadoAlteracao(){
            
            if(estadoAlteracao==1){
                jButtonNovoCliente.setEnabled(true);
                jButtonAlterarCliente.setEnabled(false);
                jButtonExcluirCliente.setEnabled(false);
                jButtonPesquisarCliente.setEnabled(true);
                jButtonSalvar.setEnabled(false);
                jButtonCancelar.setEnabled(false);
            }
            else if(estadoAlteracao==2){

                jButtonNovoCliente.setEnabled(false);
                jButtonAlterarCliente.setEnabled(false);
                jButtonExcluirCliente.setEnabled(false);
                jButtonPesquisarCliente.setEnabled(false);
                jButtonSalvar.setEnabled(true);
                jButtonCancelar.setEnabled(true);
            }
        
        }
        public void EstadoAlteracaoPedido(){
            if(estadoAlteracaoPedido==1){
                jButtonNovoPedido.setEnabled(true);
                jButtonFinalizarPedido.setEnabled(false);
                jButtonCancelarPedido.setEnabled(false);
                jTableClientePedido.setEnabled(false);
                jTablePrato.setEnabled(false);
                jTableBebida.setEnabled(false);
                jTableSobremesa.setEnabled(false);
                jButtonAdicionarPrato.setEnabled(false);
                jButtonAdicionarBebida.setEnabled(false);
                jButtonAdicionarSobremesa.setEnabled(false);
                jTextAreaObservacao.setEnabled(false);
            }
            else if(estadoAlteracaoPedido==2){
                jButtonNovoPedido.setEnabled(false);
                jButtonFinalizarPedido.setEnabled(true);
                jButtonCancelarPedido.setEnabled(true);
                jTableClientePedido.setEnabled(true);
                jTablePrato.setEnabled(true);
                jTableBebida.setEnabled(true);
                jTableSobremesa.setEnabled(true);
                jButtonAdicionarPrato.setEnabled(true);
                jButtonAdicionarBebida.setEnabled(true);
                jButtonAdicionarSobremesa.setEnabled(true);
                jTextAreaObservacao.setEnabled(true);
            }
        }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanelCliente = new javax.swing.JPanel();
        jButtonNovoCliente = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabelNomeCliente = new javax.swing.JLabel();
        jTextFieldNomeCliente = new javax.swing.JTextField();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldCpf = new javax.swing.JTextField();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonAlterarCliente = new javax.swing.JButton();
        jButtonLogOut = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableClientesCadastrados = new javax.swing.JTable();
        jButtonExcluirCliente = new javax.swing.JButton();
        jButtonPesquisarCliente = new javax.swing.JButton();
        jPanelPedido = new javax.swing.JPanel();
        jButtonNovoPedido = new javax.swing.JButton();
        jButtonCancelarPedido = new javax.swing.JButton();
        jButtonFinalizarPedido = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePedido = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableClientePedido = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTablePrato = new javax.swing.JTable();
        jButtonAdicionarPrato = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableBebida = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jButtonAdicionarBebida = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableSobremesa = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jButtonAdicionarSobremesa = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabelPrecoTotal = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTextAreaObservacao = new javax.swing.JTextArea();
        jPanelEstoque = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableListaPratos = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTableListaBebidas = new javax.swing.JTable();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTableListaSobremesas = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jButtonAdicionarNovoPrato = new javax.swing.JButton();
        jButtonRemoverPratoEstoque = new javax.swing.JButton();
        jButtonAdicionarNovaBebida = new javax.swing.JButton();
        jButtonAdicionarNovaSobremesa = new javax.swing.JButton();
        jButtonRemoverBebidaEstoque = new javax.swing.JButton();
        jButtonRemoverSobremesaEstoque = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Pedidos atuais");
        setMinimumSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jButtonNovoCliente.setText("Novo Cliente");
        jButtonNovoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoClienteActionPerformed(evt);
            }
        });

        jLabel1.setText("Clientes Cadastrados:");

        jLabelNomeCliente.setText("Nome Cliente:");

        jLabelCpf.setText("CPF:");

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonAlterarCliente.setText("Alterar");
        jButtonAlterarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAlterarClienteActionPerformed(evt);
            }
        });

        jButtonLogOut.setText("Log Out");
        jButtonLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLogOutActionPerformed(evt);
            }
        });

        jTableClientesCadastrados.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Nome", "CPF"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTableClientesCadastrados.getTableHeader().setReorderingAllowed(false);
            jTableClientesCadastrados.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTableClientesCadastradosMouseClicked(evt);
                }
            });
            jScrollPane2.setViewportView(jTableClientesCadastrados);

            jButtonExcluirCliente.setText("Excluir");
            jButtonExcluirCliente.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonExcluirClienteActionPerformed(evt);
                }
            });

            jButtonPesquisarCliente.setText("Pesquisar");
            jButtonPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonPesquisarClienteActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanelClienteLayout = new javax.swing.GroupLayout(jPanelCliente);
            jPanelCliente.setLayout(jPanelClienteLayout);
            jPanelClienteLayout.setHorizontalGroup(
                jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelClienteLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelClienteLayout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelClienteLayout.createSequentialGroup()
                            .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanelClienteLayout.createSequentialGroup()
                                    .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelClienteLayout.createSequentialGroup()
                                            .addComponent(jButtonNovoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jButtonAlterarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jButtonExcluirCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jButtonPesquisarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 107, Short.MAX_VALUE)
                                            .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelClienteLayout.createSequentialGroup()
                                            .addComponent(jLabelNomeCliente)
                                            .addGap(36, 36, 36)
                                            .addComponent(jTextFieldNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelClienteLayout.createSequentialGroup()
                                    .addGap(0, 0, Short.MAX_VALUE)
                                    .addComponent(jButtonLogOut))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelClienteLayout.createSequentialGroup()
                                    .addComponent(jLabelCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(36, 36, 36)
                                    .addComponent(jTextFieldCpf)))
                            .addGap(23, 23, 23))))
            );
            jPanelClienteLayout.setVerticalGroup(
                jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelClienteLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonNovoCliente)
                        .addComponent(jButtonCancelar)
                        .addComponent(jButtonSalvar)
                        .addComponent(jButtonAlterarCliente)
                        .addComponent(jButtonExcluirCliente)
                        .addComponent(jButtonPesquisarCliente))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel1)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelNomeCliente)
                        .addComponent(jTextFieldNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelCpf)
                        .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 274, Short.MAX_VALUE)
                    .addComponent(jButtonLogOut)
                    .addContainerGap())
            );

            jTabbedPane2.addTab("Cliente", jPanelCliente);

            jButtonNovoPedido.setText("Novo Pedido");
            jButtonNovoPedido.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonNovoPedidoActionPerformed(evt);
                }
            });

            jButtonCancelarPedido.setText("Cancelar");
            jButtonCancelarPedido.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonCancelarPedidoActionPerformed(evt);
                }
            });

            jButtonFinalizarPedido.setText("Finalizar Pedido");
            jButtonFinalizarPedido.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonFinalizarPedidoActionPerformed(evt);
                }
            });

            jLabel2.setText("Selecionar Cliente:");

            jTablePedido.setModel(new javax.swing.table.DefaultTableModel 
                (
                    null,
                    new String [] {
                        "Nome", "Preço"
                    }
                )
                {
                    @Override    
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jTablePedido.getTableHeader().setReorderingAllowed(false);
                jScrollPane1.setViewportView(jTablePedido);

                jLabel3.setText("Selecione o prato:");

                jLabel4.setText("Pedido:");

                jTableClientePedido.setModel(new javax.swing.table.DefaultTableModel 
                    (
                        null,
                        new String [] {
                            "Nome", "CPF"
                        }
                    )
                    {
                        @Override    
                        public boolean isCellEditable(int rowIndex, int mColIndex) {
                            return false;
                        }
                    });
                    jTableClientePedido.getTableHeader().setReorderingAllowed(false);
                    jTableClientePedido.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            jTableClientePedidoMouseClicked(evt);
                        }
                    });
                    jScrollPane3.setViewportView(jTableClientePedido);

                    jTablePrato.setModel(new javax.swing.table.DefaultTableModel 
                        (
                            null,
                            new String [] {
                                "Nome", "Preço"
                            }
                        )
                        {
                            @Override    
                            public boolean isCellEditable(int rowIndex, int mColIndex) {
                                return false;
                            }
                        });
                        jTablePrato.getTableHeader().setReorderingAllowed(false);
                        jTablePrato.addMouseListener(new java.awt.event.MouseAdapter() {
                            public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTablePratoMouseClicked(evt);
                            }
                        });
                        jScrollPane4.setViewportView(jTablePrato);

                        jButtonAdicionarPrato.setText("Adicionar Prato");
                        jButtonAdicionarPrato.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonAdicionarPratoActionPerformed(evt);
                            }
                        });

                        jTableBebida.setModel(new javax.swing.table.DefaultTableModel 
                            (
                                null,
                                new String [] {
                                    "Nome", "Preço"
                                }
                            )
                            {
                                @Override    
                                public boolean isCellEditable(int rowIndex, int mColIndex) {
                                    return false;
                                }
                            });
                            jTableBebida.addMouseListener(new java.awt.event.MouseAdapter() {
                                public void mouseClicked(java.awt.event.MouseEvent evt) {
                                    jTableBebidaMouseClicked(evt);
                                }
                            });
                            jScrollPane5.setViewportView(jTableBebida);

                            jLabel5.setText("Selecione a bebida:");

                            jButtonAdicionarBebida.setText("Adicionar Bebida");
                            jButtonAdicionarBebida.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                    jButtonAdicionarBebidaActionPerformed(evt);
                                }
                            });

                            jTableSobremesa.setModel(new javax.swing.table.DefaultTableModel 
                                (
                                    null,
                                    new String [] {
                                        "Nome", "Preço"
                                    }
                                )
                                {
                                    @Override    
                                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                                        return false;
                                    }
                                });
                                jTableSobremesa.addMouseListener(new java.awt.event.MouseAdapter() {
                                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                                        jTableSobremesaMouseClicked(evt);
                                    }
                                });
                                jScrollPane6.setViewportView(jTableSobremesa);

                                jLabel6.setText("Selecione a sobremesa:");

                                jButtonAdicionarSobremesa.setText("Adicionar Sobremesa");
                                jButtonAdicionarSobremesa.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jButtonAdicionarSobremesaActionPerformed(evt);
                                    }
                                });

                                jLabel10.setText("Preço Total:");

                                jLabel11.setText("Observação:");

                                jTextAreaObservacao.setColumns(20);
                                jTextAreaObservacao.setRows(5);
                                jScrollPane11.setViewportView(jTextAreaObservacao);

                                javax.swing.GroupLayout jPanelPedidoLayout = new javax.swing.GroupLayout(jPanelPedido);
                                jPanelPedido.setLayout(jPanelPedidoLayout);
                                jPanelPedidoLayout.setHorizontalGroup(
                                    jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                .addComponent(jButtonNovoPedido)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jButtonFinalizarPedido)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonCancelarPedido))
                                            .addComponent(jScrollPane3)
                                            .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel3)
                                                            .addComponent(jButtonAdicionarPrato))
                                                        .addGap(0, 128, Short.MAX_VALUE))
                                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jButtonAdicionarBebida)
                                                    .addComponent(jLabel5)
                                                    .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                        .addGap(12, 12, 12)
                                                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jButtonAdicionarSobremesa)
                                                    .addComponent(jLabel6)
                                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel2)
                                                    .addComponent(jLabel4)
                                                    .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                                        .addComponent(jLabel10)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jLabelPrecoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel11)
                                                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                        .addContainerGap())
                                );
                                jPanelPedidoLayout.setVerticalGroup(
                                    jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelPedidoLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jButtonNovoPedido)
                                            .addComponent(jButtonCancelarPedido)
                                            .addComponent(jButtonFinalizarPedido))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButtonAdicionarPrato)
                                            .addComponent(jButtonAdicionarBebida)
                                            .addComponent(jButtonAdicionarSobremesa))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanelPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabelPrecoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(32, Short.MAX_VALUE))
                                );

                                jTabbedPane2.addTab("Pedido", jPanelPedido);

                                jLabel7.setText("Lista de Pratos");

                                jTableListaPratos.setModel(new javax.swing.table.DefaultTableModel 
                                    (
                                        null,
                                        new String [] {
                                            "Nome", "Preço", "Quantidade", "Quantidade Minima"
                                        }
                                    )
                                    {
                                        @Override    
                                        public boolean isCellEditable(int rowIndex, int mColIndex) {
                                            return false;
                                        }
                                    });
                                    jTableListaPratos.addMouseListener(new java.awt.event.MouseAdapter() {
                                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                            jTableListaPratosMouseClicked(evt);
                                        }
                                    });
                                    jScrollPane7.setViewportView(jTableListaPratos);

                                    jLabel8.setText("Lista de Bebidas");

                                    jTableListaBebidas.setModel(new javax.swing.table.DefaultTableModel 
                                        (
                                            null,
                                            new String [] {
                                                "Nome", "Preço", "Quantidade", "Quantidade Minima"
                                            }
                                        )
                                        {
                                            @Override    
                                            public boolean isCellEditable(int rowIndex, int mColIndex) {
                                                return false;
                                            }
                                        });
                                        jTableListaBebidas.getTableHeader().setReorderingAllowed(false);
                                        jTableListaBebidas.addMouseListener(new java.awt.event.MouseAdapter() {
                                            public void mouseClicked(java.awt.event.MouseEvent evt) {
                                                jTableListaBebidasMouseClicked(evt);
                                            }
                                        });
                                        jScrollPane8.setViewportView(jTableListaBebidas);

                                        jTableListaSobremesas.setModel(new javax.swing.table.DefaultTableModel 
                                            (
                                                null,
                                                new String [] {
                                                    "Nome", "Preço", "Quantidade", "Quantidade Minima"
                                                }
                                            )
                                            {
                                                @Override    
                                                public boolean isCellEditable(int rowIndex, int mColIndex) {
                                                    return false;
                                                }
                                            });
                                            jTableListaSobremesas.getTableHeader().setReorderingAllowed(false);
                                            jTableListaSobremesas.addMouseListener(new java.awt.event.MouseAdapter() {
                                                public void mouseClicked(java.awt.event.MouseEvent evt) {
                                                    jTableListaSobremesasMouseClicked(evt);
                                                }
                                            });
                                            jScrollPane9.setViewportView(jTableListaSobremesas);

                                            jLabel9.setText("Lista de Sobremesas");

                                            jButtonAdicionarNovoPrato.setText("Adicionar novo prato");
                                            jButtonAdicionarNovoPrato.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonAdicionarNovoPratoActionPerformed(evt);
                                                }
                                            });

                                            jButtonRemoverPratoEstoque.setText("Remover Prato do Estoque");
                                            jButtonRemoverPratoEstoque.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonRemoverPratoEstoqueActionPerformed(evt);
                                                }
                                            });

                                            jButtonAdicionarNovaBebida.setText("Adicionar nova bebida");
                                            jButtonAdicionarNovaBebida.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonAdicionarNovaBebidaActionPerformed(evt);
                                                }
                                            });

                                            jButtonAdicionarNovaSobremesa.setText("Adicionar nova sobremesa");
                                            jButtonAdicionarNovaSobremesa.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonAdicionarNovaSobremesaActionPerformed(evt);
                                                }
                                            });

                                            jButtonRemoverBebidaEstoque.setText("Remover bebida do Estoque");
                                            jButtonRemoverBebidaEstoque.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonRemoverBebidaEstoqueActionPerformed(evt);
                                                }
                                            });

                                            jButtonRemoverSobremesaEstoque.setText("Remover sobremesa do Estoque");
                                            jButtonRemoverSobremesaEstoque.addActionListener(new java.awt.event.ActionListener() {
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    jButtonRemoverSobremesaEstoqueActionPerformed(evt);
                                                }
                                            });

                                            javax.swing.GroupLayout jPanelEstoqueLayout = new javax.swing.GroupLayout(jPanelEstoque);
                                            jPanelEstoque.setLayout(jPanelEstoqueLayout);
                                            jPanelEstoqueLayout.setHorizontalGroup(
                                                jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanelEstoqueLayout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addGroup(jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jLabel7)
                                                        .addComponent(jLabel8)
                                                        .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                                                        .addComponent(jLabel9)
                                                        .addComponent(jScrollPane8)
                                                        .addComponent(jScrollPane9))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addGroup(jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jButtonRemoverSobremesaEstoque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonAdicionarNovaSobremesa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonRemoverBebidaEstoque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonAdicionarNovaBebida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonAdicionarNovoPrato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonRemoverPratoEstoque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                    .addContainerGap(67, Short.MAX_VALUE))
                                            );
                                            jPanelEstoqueLayout.setVerticalGroup(
                                                jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanelEstoqueLayout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addComponent(jLabel7)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelEstoqueLayout.createSequentialGroup()
                                                            .addComponent(jButtonAdicionarNovoPrato)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(jButtonRemoverPratoEstoque)))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel8)
                                                    .addGap(11, 11, 11)
                                                    .addGroup(jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelEstoqueLayout.createSequentialGroup()
                                                            .addComponent(jButtonAdicionarNovaBebida)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(jButtonRemoverBebidaEstoque)))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel9)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(jPanelEstoqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelEstoqueLayout.createSequentialGroup()
                                                            .addComponent(jButtonAdicionarNovaSobremesa)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(jButtonRemoverSobremesaEstoque)))
                                                    .addContainerGap(50, Short.MAX_VALUE))
                                            );

                                            jTabbedPane2.addTab("Estoque", jPanelEstoque);

                                            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                                            getContentPane().setLayout(layout);
                                            layout.setHorizontalGroup(
                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                                            );
                                            layout.setVerticalGroup(
                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                                            );

                                            pack();
                                            setLocationRelativeTo(null);
                                        }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        //this.callerJFrame.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void jButtonRemoverSobremesaEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverSobremesaEstoqueActionPerformed
        controlDessert.remove(dessertSelect);
        controlDessert.escreverArquivoSobremesas();
        carregarProductList();
    }//GEN-LAST:event_jButtonRemoverSobremesaEstoqueActionPerformed

    private void jButtonRemoverBebidaEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverBebidaEstoqueActionPerformed
        controlDrink.remove(drinkSelect);
        controlDrink.escreverArquivoBebidas();
        carregarProductList();
    }//GEN-LAST:event_jButtonRemoverBebidaEstoqueActionPerformed

    private void jButtonAdicionarNovaSobremesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarNovaSobremesaActionPerformed
        dessert = new Dessert();
        String nome = JOptionPane.showInputDialog("Informe o Nome da sobremesa:");
        String price = JOptionPane.showInputDialog("Informe o preço da sobremesa:");
        String quantidade = JOptionPane.showInputDialog("Informe a quantidade da sobremesa:");
        String quantidadeMinima = JOptionPane.showInputDialog("Informe a quantidade minima de sobremesas:");
        Stock stock = new Stock();
        dessert.setName(nome);
        dessert.setPrice(Double.parseDouble(price));
        stock.setAmount(Integer.valueOf(quantidade));
        stock.setMinimumAmount(Integer.valueOf(quantidadeMinima));
        dessert.setStock(stock);
        controlDessert.add(dessert);
        controlDessert.escreverArquivoSobremesas();
        this.carregarProductList();
    }//GEN-LAST:event_jButtonAdicionarNovaSobremesaActionPerformed

    private void jButtonAdicionarNovaBebidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarNovaBebidaActionPerformed
        drink = new Drink();
        String nome = JOptionPane.showInputDialog("Informe o Nome da bebida:");
        String price = JOptionPane.showInputDialog("Informe o preço da bebida:");
        String quantidade = JOptionPane.showInputDialog("Informe a quantidade da bebida:");
        String quantidadeMinima = JOptionPane.showInputDialog("Informe a quantidade minima de bebidas:");
        Stock stock = new Stock();
        drink.setName(nome);
        drink.setPrice(Double.parseDouble(price));
        stock.setAmount(Integer.valueOf(quantidade));
        stock.setMinimumAmount(Integer.valueOf(quantidadeMinima));
        drink.setStock(stock);
        controlDrink.add(drink);
        controlDrink.escreverArquivoBebidas();
        this.carregarProductList();
    }//GEN-LAST:event_jButtonAdicionarNovaBebidaActionPerformed

    private void jButtonRemoverPratoEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverPratoEstoqueActionPerformed
        controlDish.remove(dishSelect);
        controlDish.escreverArquivoPratos();
        carregarProductList();
    }//GEN-LAST:event_jButtonRemoverPratoEstoqueActionPerformed

    private void jButtonAdicionarNovoPratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarNovoPratoActionPerformed
        dish = new Dish();
        String nome = JOptionPane.showInputDialog("Informe o Nome do prato:");
        String price = JOptionPane.showInputDialog("Informe o preço do prato:");
        String quantidade = JOptionPane.showInputDialog("Informe a quantidade do prato:");
        String quantidadeMinima = JOptionPane.showInputDialog("Informe a quantidade minima de pratos:");
        Stock stock = new Stock();
        dish.setName(nome);
        dish.setPrice(Double.parseDouble(price));
        stock.setAmount(Integer.valueOf(quantidade));
        stock.setMinimumAmount(Integer.valueOf(quantidadeMinima));
        dish.setStock(stock);
        controlDish.add(dish);
        controlDish.escreverArquivoPratos();
        this.carregarProductList();
    }//GEN-LAST:event_jButtonAdicionarNovoPratoActionPerformed

    private void jTableListaSobremesasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaSobremesasMouseClicked
        if (jTableListaSobremesas.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaSobremesas.getModel();
            String nome = (String) model.getValueAt(jTableListaSobremesas.getSelectedRow(), 0);

            dessertSelect = controlDessert.Search(nome);

        }
    }//GEN-LAST:event_jTableListaSobremesasMouseClicked

    private void jTableListaBebidasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaBebidasMouseClicked
        if (jTableListaBebidas.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaBebidas.getModel();
            String nome = (String) model.getValueAt(jTableListaBebidas.getSelectedRow(), 0);

            drinkSelect = controlDrink.Search(nome);

        }
    }//GEN-LAST:event_jTableListaBebidasMouseClicked

    private void jTableListaPratosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaPratosMouseClicked
        if (jTableListaPratos.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaPratos.getModel();
            String nome = (String) model.getValueAt(jTableListaPratos.getSelectedRow(), 0);

            dishSelect = controlDish.Search(nome);

        }
    }//GEN-LAST:event_jTableListaPratosMouseClicked

    private void jButtonAdicionarSobremesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarSobremesaActionPerformed
        if (jTableSobremesa.isEnabled()) {

            if(clicouSobremesa==false){
                JOptionPane.showMessageDialog(this, "Selecione um produto para adicionar ao pedido");
            }
            else{

                Stock stock = dessert.getStock();
                Integer quantidade = stock.getAmount();

                if((stock.getAmount())>stock.getMinimumAmount()){
                    order.addDessert(dessert);
                    stock.setAmount(quantidade-1);
                }
                else{
                    JOptionPane.showMessageDialog(this, "Quantidade minima de produto atingida. Não é possível adicionar produto");
                }

                CarregarListaPedido();
                clicouSobremesa=false;
            }

        }
    }//GEN-LAST:event_jButtonAdicionarSobremesaActionPerformed

    private void jTableSobremesaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableSobremesaMouseClicked
        if (jTableSobremesa.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableSobremesa.getModel();
            String nome = (String) model.getValueAt(jTableSobremesa.getSelectedRow(), 0);
            dessert = new Dessert();
            dessert = controlDessert.Search(nome);
            clicouSobremesa=true;
        }
    }//GEN-LAST:event_jTableSobremesaMouseClicked

    private void jButtonAdicionarBebidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarBebidaActionPerformed
        if (jTableBebida.isEnabled()) {

            if(clicouBebida==false){
                JOptionPane.showMessageDialog(this, "Selecione um produto para adicionar ao pedido");
            }
            else{

                Stock stock = drink.getStock();
                Integer quantidade = stock.getAmount();

                if((stock.getAmount())>stock.getMinimumAmount()){
                    order.addDrink(drink);
                    stock.setAmount(quantidade-1);
                }
                else{
                    JOptionPane.showMessageDialog(this, "Quantidade minima de produto atingida. Não é possível adicionar produto");
                }

                CarregarListaPedido();
                clicouBebida=false;
            }

        }
    }//GEN-LAST:event_jButtonAdicionarBebidaActionPerformed

    private void jTableBebidaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableBebidaMouseClicked
        if (jTableBebida.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableBebida.getModel();
            String nome = (String) model.getValueAt(jTableBebida.getSelectedRow(), 0);
            drink = new Drink();
            drink = controlDrink.Search(nome);
            clicouBebida=true;
        }
    }//GEN-LAST:event_jTableBebidaMouseClicked

    private void jButtonAdicionarPratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarPratoActionPerformed
        if (jTablePrato.isEnabled()) {

            if(clicouPrato==false){
                JOptionPane.showMessageDialog(this, "Selecione um produto para adicionar ao pedido");
            }
            else{

                Stock stock = dish.getStock();
                Integer quantidade = stock.getAmount();

                if((stock.getAmount())>stock.getMinimumAmount()){
                    order.addDish(dish);
                    stock.setAmount(quantidade-1);
                }
                else{
                    JOptionPane.showMessageDialog(this, "Quantidade minima de produto atingida. Não é possível adicionar produto");
                }

                CarregarListaPedido();
                clicouPrato=false;
            }

        }
    }//GEN-LAST:event_jButtonAdicionarPratoActionPerformed

    private void jTablePratoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePratoMouseClicked
        if (jTablePrato.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTablePrato.getModel();
            String nome = (String) model.getValueAt(jTablePrato.getSelectedRow(), 0);
            dish = new Dish();
            dish = controlDish.Search(nome);
            clicouPrato=true;
        }
    }//GEN-LAST:event_jTablePratoMouseClicked

    private void jTableClientePedidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableClientePedidoMouseClicked

        if (jTableClientePedido.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableClientePedido.getModel();
            String nome = (String) model.getValueAt(jTableClientePedido.getSelectedRow(), 0);
            clientePedido=controlClient.Search(nome);
            order.setClient(clientePedido);
            //order.setDish(nome);

        }
    }//GEN-LAST:event_jTableClientePedidoMouseClicked

    private void jButtonFinalizarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFinalizarPedidoActionPerformed
        if(order.getClient()==null){
            JOptionPane.showMessageDialog(this, "É necessário selecionar um cliente");
        }
        else if(order.getDish().isEmpty() && order.getDrink().isEmpty() && order.getDessert().isEmpty()){
            JOptionPane.showMessageDialog(this, "É necessário adicionar um produto");
        }
        else{
            controlOrder.add(order);
            ArrayList<Dish> dishList = new ArrayList<Dish>();
            ArrayList<Drink> drinkList = new ArrayList<Drink>();
            ArrayList<Dessert> dessertList = new ArrayList<Dessert>();

            dishList = order.getDish();
            drinkList = order.getDrink();
            dessertList = order.getDessert();

            for(Dish a : dishList){
                controlDish.remove(a);
                controlDish.add(a);
                controlDish.escreverArquivoPratos();
            }
            for(Drink a : drinkList){
                controlDrink.remove(a);
                controlDrink.add(a);
                controlDrink.escreverArquivoBebidas();
            }
            for(Dessert a : dessertList){
                controlDessert.remove(a);
                controlDessert.add(a);
                controlDessert.escreverArquivoSobremesas();
            }
            String note = jTextAreaObservacao.getText();
            order.setNote(note);
            order.setPaidOut(false);
            boolean verificaPagamento;

            final PaymentView paymentView = new PaymentView(order, controlOrder);
            paymentView.setVisible(true);

            estadoAlteracaoPedido=1;
        EstadoAlteracaoPedido();
        DefaultTableModel model = (DefaultTableModel) jTablePedido.getModel();
        model.setRowCount(0);
        carregarProductList();
        dish= new Dish();
        drink=new Drink();
        dessert=new Dessert();
        jLabelPrecoTotal.setText(null);
        jTextAreaObservacao.setText(" ");

            //final PaymentView paymentView = new PaymentView(order);
            // paymentView.setVisible(true);
        }
    }//GEN-LAST:event_jButtonFinalizarPedidoActionPerformed

    private void jButtonCancelarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarPedidoActionPerformed
        estadoAlteracaoPedido=1;
        EstadoAlteracaoPedido();
        DefaultTableModel model = (DefaultTableModel) jTablePedido.getModel();
        model.setRowCount(0);
        carregarProductList();
        dish= new Dish();
        drink=new Drink();
        dessert=new Dessert();
        jLabelPrecoTotal.setText(null);
        jTextAreaObservacao.setText(" ");
    }//GEN-LAST:event_jButtonCancelarPedidoActionPerformed

    private void jButtonNovoPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoPedidoActionPerformed
        //order = null;
        order = new Order();
        modoAlteracaoPedido=true;
        novoPedido = true;
        estadoAlteracaoPedido=2;
        EstadoAlteracaoPedido();
        jTextAreaObservacao.setText(" ");
    }//GEN-LAST:event_jButtonNovoPedidoActionPerformed

    private void jButtonPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarClienteActionPerformed
        String pesquisa = JOptionPane.showInputDialog("Informe o nome do Cliente:");
        if(pesquisa!=null){
            this.pesquisarCliente(pesquisa);
        }
    }//GEN-LAST:event_jButtonPesquisarClienteActionPerformed

    private void jButtonExcluirClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirClienteActionPerformed
        controlClient.remove(client);
        client = null;
        this.limparCampos();

        //this.HabilitarDesabilitarCampos();
        controlClient.escreverArquivoCliente();
        this.carregarClientList();
    }//GEN-LAST:event_jButtonExcluirClienteActionPerformed

    private void jTableClientesCadastradosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableClientesCadastradosMouseClicked
        if (jTableClientesCadastrados.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableClientesCadastrados.getModel();
            String nome = (String) model.getValueAt(jTableClientesCadastrados.getSelectedRow(), 0);
            this.pesquisarCliente(nome);
            jButtonAlterarCliente.setEnabled(true);
            jButtonExcluirCliente.setEnabled(true);
        }
    }//GEN-LAST:event_jTableClientesCadastradosMouseClicked

    private void jButtonLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLogOutActionPerformed
        dispose();
        this.callerJFrame.setVisible(true);
    }//GEN-LAST:event_jButtonLogOutActionPerformed

    private void jButtonAlterarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAlterarClienteActionPerformed
        modoAlteracao = true;
        novoRegistro = false;
        estadoAlteracao=2;
        EstadoAlteracao();
        this.HabilitarDesabilitarCampos();
        this.jTextFieldNomeCliente.requestFocus();
    }//GEN-LAST:event_jButtonAlterarClienteActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        modoAlteracao=false;
        salvarRegistro();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        if(novoRegistro = true){
            this.limparCampos();
        }
        else{
            this.preencherCampos();
        }
        modoAlteracao=false;
        novoRegistro = false;
        estadoAlteracao=1;
        EstadoAlteracao();
        this.HabilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonNovoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoClienteActionPerformed
        client = null;
        modoAlteracao=true;
        novoRegistro = true;
        estadoAlteracao=2;
        limparCampos();
        HabilitarDesabilitarCampos();
        EstadoAlteracao();
        jTextFieldNomeCliente.requestFocus();
    }//GEN-LAST:event_jButtonNovoClienteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarBebida;
    private javax.swing.JButton jButtonAdicionarNovaBebida;
    private javax.swing.JButton jButtonAdicionarNovaSobremesa;
    private javax.swing.JButton jButtonAdicionarNovoPrato;
    private javax.swing.JButton jButtonAdicionarPrato;
    private javax.swing.JButton jButtonAdicionarSobremesa;
    private javax.swing.JButton jButtonAlterarCliente;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonCancelarPedido;
    private javax.swing.JButton jButtonExcluirCliente;
    private javax.swing.JButton jButtonFinalizarPedido;
    private javax.swing.JButton jButtonLogOut;
    private javax.swing.JButton jButtonNovoCliente;
    private javax.swing.JButton jButtonNovoPedido;
    private javax.swing.JButton jButtonPesquisarCliente;
    private javax.swing.JButton jButtonRemoverBebidaEstoque;
    private javax.swing.JButton jButtonRemoverPratoEstoque;
    private javax.swing.JButton jButtonRemoverSobremesaEstoque;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelNomeCliente;
    private javax.swing.JLabel jLabelPrecoTotal;
    private javax.swing.JPanel jPanelCliente;
    private javax.swing.JPanel jPanelEstoque;
    private javax.swing.JPanel jPanelPedido;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTableBebida;
    private javax.swing.JTable jTableClientePedido;
    private javax.swing.JTable jTableClientesCadastrados;
    private javax.swing.JTable jTableListaBebidas;
    private javax.swing.JTable jTableListaPratos;
    private javax.swing.JTable jTableListaSobremesas;
    private javax.swing.JTable jTablePedido;
    private javax.swing.JTable jTablePrato;
    private javax.swing.JTable jTableSobremesa;
    private javax.swing.JTextArea jTextAreaObservacao;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldNomeCliente;
    // End of variables declaration//GEN-END:variables
}
