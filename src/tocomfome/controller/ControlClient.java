
package tocomfome.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import tocomfome.models.Client;


public class ControlClient {
    
    private ArrayList<Client> clientList;
    
    public ControlClient(){
        this.clientList = new ArrayList<Client>();
    }

    public ArrayList<Client> getClientList() {
        return clientList;
    }
    
    public void add(Client aClient){
        clientList.add(aClient);
    }
    public void remove(Client aClient){
        clientList.remove(aClient);
    }
    public Client Search(String name){
        for(Client b: clientList){
            if(b.getName().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    public void escreverArquivoCliente(){
        
        try {

            File file = new File("src/tocomfome/data/Clientes.txt");

		
		if (!file.exists()) {
                    file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(Client clt: clientList){
                    bw.write(clt.getName() + "," + clt.getCpf());
                    bw.write("\n");
		
                }
                bw.flush();
                bw.close();

	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
    public void lerArquivoCliente(){
        ArrayList<Client> clientesLidos;
        clientesLidos = new ArrayList<Client>();
        
        Client client = null;
        
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/tocomfome/data/Clientes.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            client = new Client();
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String cpf = parts[1];

            client.setName(nome);
            client.setCpf(cpf);
            
            clientesLidos.add(client);
            
        
        }
        clientList = (ArrayList<Client>) clientesLidos.clone();

    }
}