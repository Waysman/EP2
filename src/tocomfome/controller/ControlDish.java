
package tocomfome.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Scanner;
import static javax.xml.bind.DatatypeConverter.parseInteger;
import tocomfome.models.Dish;
import tocomfome.models.Stock;


public class ControlDish {
    
    private ArrayList<Dish> dishList;
    
    public ControlDish(){
        this.dishList = new ArrayList<Dish>();
    }
    public ArrayList<Dish> getDishList(){
        return dishList;
    }
    public void add(Dish dish){
        dishList.add(dish);
    }
    public void remove(Dish dish){
        dishList.remove(dish);
    }
    public Dish Search(String name){
        for(Dish b: dishList){
            if(b.getName().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    public void lerArquivoPratos(){
        Stock stock;
        Dish dish = null;
        ArrayList<Dish> dishListTemp = new ArrayList<Dish>();
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/tocomfome/data/Pratos.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            dish = new Dish();
            stock = new Stock();
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String preco = parts[1];
            String amount = parts[2];
            String minimumAmount = parts[3];
            dish.setName(nome);
            dish.setPrice(parseDouble(preco));
            stock.setAmount(Integer.valueOf(amount));
            stock.setMinimumAmount(Integer.valueOf(minimumAmount));
            dish.setStock(stock);
            
            dishListTemp.add(dish);
        }
        dishList = (ArrayList<Dish>) dishListTemp.clone();
        
        
    }
    public void escreverArquivoPratos(){
        
        try {

            File file = new File("src/tocomfome/data/Pratos.txt");
            //newStock = new Stock();
		
            if (!file.exists()) {
                file.createNewFile();
            }

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(Dish clt: dishList){
                    Stock newStock = new Stock();
                    newStock = clt.getStock();
                    
                    bw.write(clt.getName() + "," + clt.getPrice() + "," + newStock.getAmount().toString() + "," + newStock.getMinimumAmount().toString());
                    bw.write("\n");
		
                }
                
                bw.flush();
                bw.close();


	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
}
