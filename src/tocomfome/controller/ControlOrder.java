
package tocomfome.controller;

import java.util.ArrayList;
import tocomfome.models.Order;


public class ControlOrder {
    
    private ArrayList<Order> orderList;
    
    public ControlOrder(){
        this.orderList = new ArrayList<Order>();
    }
    public ArrayList<Order>getorderList(){
        return orderList;
    }
    public void add(Order anOrder){
        orderList.add(anOrder);
    }
    public void remove(Order anOrder){
        orderList.remove(anOrder);
    }
    
}
