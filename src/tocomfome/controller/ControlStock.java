
package tocomfome.controller;

import java.util.ArrayList;
import tocomfome.models.Stock;


public class ControlStock {
    private ArrayList<Stock> stockList;
    
    public ControlStock(){
        this.stockList = new ArrayList<Stock>();
    }
    public ArrayList<Stock> getStockList(){
        return stockList;
    }
    public void add(Stock stock){
        stockList.add(stock);
    }
    public void remove(Stock stock){
        stockList.remove(stock);
    }
}
