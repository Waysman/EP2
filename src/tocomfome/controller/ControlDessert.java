
package tocomfome.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Scanner;
import tocomfome.models.Dessert;
import tocomfome.models.Stock;


public class ControlDessert {
    private ArrayList<Dessert> dessertList;
    
    public ControlDessert(){
        this.dessertList = new ArrayList<Dessert>();
    }
    public ArrayList<Dessert> getDessertList(){
        return dessertList;
    }
    public void add(Dessert dessert){
        dessertList.add(dessert);
    }
    public void remove(Dessert dessert){
        dessertList.remove(dessert);
    }
    public Dessert Search(String name){
        for(Dessert b: dessertList){
            if(b.getName().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    public void lerArquivoSobremesas(){
        Stock stock;
        Dessert dessert = null;
        ArrayList<Dessert> dessertListTemp = new ArrayList<Dessert>();
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/tocomfome/data/Sobremesas.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            dessert = new Dessert();
            stock  = new Stock();
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String preco = parts[1];
            String amount = parts[2];
            String minimumAmount = parts[3];
            dessert.setName(nome);
            dessert.setPrice(parseDouble(preco));
            stock.setAmount(Integer.valueOf(amount));
            stock.setMinimumAmount(Integer.valueOf(minimumAmount));
            dessert.setStock(stock);
            
            dessertListTemp.add(dessert);
        }
        dessertList =  (ArrayList<Dessert>) dessertListTemp.clone();
        
    }
    public void escreverArquivoSobremesas(){
        
        try {

            File file = new File("src/tocomfome/data/Sobremesas.txt");
            
		
            if (!file.exists()) {
                file.createNewFile();
            }

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(Dessert clt: dessertList){
                    Stock newStock = new Stock();
                    newStock = clt.getStock();
                    
                    bw.write(clt.getName() + "," + clt.getPrice() + "," + newStock.getAmount().toString() + "," + newStock.getMinimumAmount().toString());
                    bw.write("\n");
		
                }
                
                bw.flush();
                bw.close();


	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
}
