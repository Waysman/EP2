
package tocomfome.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Scanner;
import tocomfome.models.Drink;
import tocomfome.models.Stock;


public class ControlDrink {
    
    private ArrayList<Drink> drinkList;
    
    public ControlDrink(){
        this.drinkList = new ArrayList<Drink>();
    }
    public ArrayList<Drink> getDrinkList(){
        return drinkList;
    }
    public void add(Drink drink){
        drinkList.add(drink);
    }
    public void remove(Drink drink){
        drinkList.remove(drink);
    }
    public Drink Search(String name){
        for(Drink b: drinkList){
            if(b.getName().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    public void lerArquivoBebidas(){
        Stock stock;
        Drink drink = null;
        ArrayList<Drink> drinkListTemp = new ArrayList<Drink>();
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/tocomfome/data/Bebidas.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            drink = new Drink();
            stock = new Stock();
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String preco = parts[1];
            String amount = parts[2];
            String minimumAmount = parts[3];
            drink.setName(nome);
            drink.setPrice(parseDouble(preco));
            stock.setAmount(Integer.valueOf(amount));
            stock.setMinimumAmount(Integer.valueOf(minimumAmount));
            drink.setStock(stock);
            
            drinkListTemp.add(drink);
        }
        drinkList = (ArrayList<Drink>) drinkListTemp.clone();
        
        
    }
    public void escreverArquivoBebidas(){
        
        try {

            File file = new File("src/tocomfome/data/Bebidas.txt");
            //newStock = new Stock();
		
            if (!file.exists()) {
                file.createNewFile();
            }

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(Drink clt: drinkList){
                    Stock newStock = new Stock();
                    newStock = clt.getStock();
                    
                    bw.write(clt.getName() + "," + clt.getPrice() + "," + newStock.getAmount().toString() + "," + newStock.getMinimumAmount().toString());
                    bw.write("\n");
		
                }
                
                bw.flush();
                bw.close();


	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
    
}
