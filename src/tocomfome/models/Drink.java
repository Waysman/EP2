
package tocomfome.models;


public class Drink extends Product{
    
    private String size;
    
    public Drink(String name){
        super(name);
    }
    public Drink(){
        super();
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    
}
