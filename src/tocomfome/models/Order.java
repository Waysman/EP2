
package tocomfome.models;

import java.util.ArrayList;
import java.util.Date;


public class Order {
    
    private Date date;
    private Double totalValue;
    private Client client;
    private ArrayList<Dish> dish;
    private ArrayList<Drink> drink;
    private ArrayList<Dessert> dessert;
    private String Note;
    private boolean paidOut;
    
    public Order(){
        dish = new ArrayList<Dish>();
        drink = new ArrayList<Drink>();
        dessert = new ArrayList<Dessert>();
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ArrayList<Dish> getDish() {
        return dish;
    }
    public void addDish(Dish dish){
        this.dish.add(dish);
    }

    public ArrayList<Drink> getDrink() {
        return drink;
    }
    public void addDrink(Drink drink){
        this.drink.add(drink);
    }

    public ArrayList<Dessert> getDessert() {
        return dessert;
    }
    public void addDessert(Dessert dessert){
        this.dessert.add(dessert);
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public boolean isPaidOut() {
        return paidOut;
    }

    public void setPaidOut(boolean paidOut) {
        this.paidOut = paidOut;
    }
    
    
    
    
    
    
}
