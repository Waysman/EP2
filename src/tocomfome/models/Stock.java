
package tocomfome.models;


public class Stock {
    
    private Integer amount;
    private Integer minimumAmount;
    
    public Stock(){
        this.amount = new Integer(0);
        this.minimumAmount = new Integer(0);
        
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     *
     * @return
     */
    public Integer getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Integer minimumAmount) {
        this.minimumAmount = minimumAmount;
    }
    
    
}
