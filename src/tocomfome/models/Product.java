
package tocomfome.models;


public class Product {
    
    private String name;
    private Double price;
    private Stock stock;

    public Product(String name){
        this.name=name;
    }
    public Product(String name, Double price){
        this.name=name;
        this.price=price;
    }
    public Product(){
        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
    
    
}
