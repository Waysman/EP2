
package tocomfome.models;

public abstract class Person {
    private String name;
    private String Cpf;

    public Person() {
    
    }

    public Person (String name){
        this.setName(name);
    }
    public Person(String name, String Cpf) {
        this.setName(name);
        this.setCpf(Cpf);
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getCpf() {
        return Cpf;
    }

    public void setCpf(String Cpf) {
        this.Cpf = Cpf;
    }
    

}
