
package tocomfome.models;


public class Dessert extends Product{
    
    public Dessert(String name){
        super(name);
    }

    public Dessert() {
        super();
    }
}
